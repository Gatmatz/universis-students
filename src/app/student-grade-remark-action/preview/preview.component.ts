import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {AngularDataContext} from '@themost/angular';
import {ErrorService, LoadingService, ModalService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProfileService} from '../../profile/services/profile.service';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html'
})
export class PreviewComponent implements OnInit, OnDestroy {

  public subscription: Subscription;
  public requestsCount: string;
  public maxNumberOfRemarking: string;
  public loading = true;

  constructor(private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _profileService: ProfileService,
              private _loadingService: LoadingService) { }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  async ngOnInit() {
    this._loadingService.showLoading();
    const student = await this._profileService.getStudent();
    const studentGradeRemarkActions = await this._context.model('StudentRequestActions')
      .asQueryable()
      .where('additionalType').equal('StudentGradeRemarkAction')
      .prepare()
      .and('actionStatus/alternateName').equal('ActiveActionStatus')
      .or('actionStatus/alternateName').equal('CompletedActionStatus')
      .getItems();

    this.maxNumberOfRemarking = student && student.studyProgram ? student.studyProgram.maxNumberOfRemarking : '-';
    this.requestsCount = studentGradeRemarkActions ? studentGradeRemarkActions.length : '-';
    this._loadingService.hideLoading();
  }

}
